//
//  BoundingBox.swift
//  TROSA
//
//  Created by Naveen on 25/09/19.
//  Copyright © 2019 HopeResearch. All rights reserved.
//

import Foundation
import UIKit


public class BoundingBox{
    
    var foregroundColor:CGColor!
    var fontSize:CGFloat!
    var font:UIFont!
    var backgroundColor:CGColor!
    var textWidth:CGFloat!
    var textHeight:CGFloat!
    var cornerRadius:CGFloat? = 8//default value
    var lineWidth:CGFloat? = 3//default value
    
    // MARK: - Bounding box initiation
    init(_ foregroundColor: CGColor,_ fontSize: CGFloat,_ backgroundColor: CGColor,_ font: UIFont,_ textWidth: CGFloat,_ textHeight: CGFloat,_ cornerRadius: CGFloat,_ lineWidth: CGFloat){
        
        self.foregroundColor =  foregroundColor
        self.fontSize = fontSize
        self.font = font
        self.textWidth = textWidth
        self.textHeight = textHeight
        self.backgroundColor = backgroundColor
        self.cornerRadius = cornerRadius
        self.lineWidth = lineWidth
        
    }
    
     // MARK: - Handling Bounding Box Placement
    func createTextSubLayer(_ bounds: CGRect,_ identifier: String,_ x: CGFloat,_ y: CGFloat) -> CALayer{
        //the below function will return CATextLayer with specified co-ordinates and  string
        var textLayer = CATextLayer()
        textLayer = CATextLayer()
        textLayer.foregroundColor = foregroundColor
        textLayer.isHidden = true
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.fontSize = fontSize
        textLayer.font = font
        textLayer.alignmentMode = CATextLayerAlignmentMode.center
        textLayer.string = "\(identifier)"
        textLayer.backgroundColor = backgroundColor
        textLayer.isHidden = false
        let attributes = [
            NSAttributedString.Key.font: textLayer.font as Any
        ]
        let textRect = "\(identifier)".boundingRect(with: CGSize(width: textWidth, height: textHeight),
                                                    options: .truncatesLastVisibleLine,
                                                    attributes: attributes, context: nil)
        let textSize = CGSize(width: textRect.width + 12, height: textRect.height)
        let textOrigin = CGPoint(x: x, y: y-textRect.height)
        textLayer.frame = CGRect(origin: textOrigin, size: textSize)
        return textLayer
    }
    
    func createRoundedRectSubLayer(_ bounds: CGRect,_ x: CGFloat,_ y: CGFloat) -> CALayer{
        //the below function will return CAShapeLayer with specified co-ordinates 
        let layer = CAShapeLayer()
        layer.bounds = bounds
        layer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius!).cgPath
        layer.lineWidth = lineWidth!
        layer.strokeColor = backgroundColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize.zero
        layer.fillColor = UIColor.clear.cgColor
        layer.position = CGPoint(x:x, y: y)
        return layer
    }
    
}
