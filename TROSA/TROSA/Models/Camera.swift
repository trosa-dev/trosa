////
//  //  AVCapture.swift
////  TROSA
////
////  Created by Naveen on 25/09/19.
////  Copyright © 2019 HopeResearch. All rights reserved.
////

import UIKit
import AVFoundation
import CoreVideo

//create a delegate class for the passing for initializing the AVCAptureDelegate in ViewController
public protocol VideoCaptureDelegate: class {
    func videoCapture(_ capture: Camera, didCaptureVideoFrame: CVPixelBuffer?, timestamp: CMTime)
}

public class Camera: NSObject {
    //Initialiaze all the AVCapturePreview  layer and delegate
    public var previewLayer: AVCaptureVideoPreviewLayer?
    public weak var delegate: VideoCaptureDelegate?
    let captureSession = AVCaptureSession()
    let videoOutput = AVCaptureVideoDataOutput()
    let queue = DispatchQueue(label: "com.TROSA.ios.TROSA")
    
    public func setUp(sessionPreset: AVCaptureSession.Preset = .medium) -> Bool {
        //iniit function for the camera session
        let success = self.setUpCamera(sessionPreset: sessionPreset)
        return success
    }
    
    func setUpCamera(sessionPreset: AVCaptureSession.Preset) -> Bool {
        
        //Setting up the camera configuration
        captureSession.beginConfiguration()
        captureSession.sessionPreset = sessionPreset
        //CHecking whether device is present or not
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Error: no video devices available")
            return false
        }
        guard let videoInput = try? AVCaptureDeviceInput(device: captureDevice) else {
            print("Error: could not create AVCaptureDeviceInput")
            return false
        }
        if captureSession.canAddInput(videoInput) {
            captureSession.addInput(videoInput)
        }
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        previewLayer.connection?.videoOrientation = .portrait
        self.previewLayer = previewLayer
        let settings: [String : Any] = [
            kCVPixelBufferPixelFormatTypeKey as String: NSNumber(value: kCVPixelFormatType_32BGRA),
        ]
        videoOutput.videoSettings = settings
        videoOutput.alwaysDiscardsLateVideoFrames = true
        videoOutput.setSampleBufferDelegate(self, queue: queue)
        if captureSession.canAddOutput(videoOutput) {
            captureSession.addOutput(videoOutput)
        }
        // We want the buffers to be in portrait orientation otherwise they are
        // rotated by 90 degrees. Need to set this _after_ addOutput()!
        videoOutput.connection(with: AVMediaType.video)?.videoOrientation = .portrait
        captureSession.commitConfiguration()
        return true
    }
    
    public func start() {
        if !captureSession.isRunning {
            captureSession.startRunning()
        }
    }
    
    public func stop() {
        if captureSession.isRunning {
            captureSession.stopRunning()
        }
    }
    
}

extension Camera: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        let timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
        //passing the data via delegate to the Function so that prediction is handled
        delegate?.videoCapture(self, didCaptureVideoFrame: pixelBuffer, timestamp: timestamp)
    }
    
    public func captureOutput(_ output: AVCaptureOutput, didDrop sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        //print("dropped frame")
    }
    
}
