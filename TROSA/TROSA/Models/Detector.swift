////
////  Detector.swift
////  TROSA
////
////  Created by Naveen on 25/09/19.
////  Copyright © 2019 HopeResearch. All rights reserved.
////
//
import Foundation
import UIKit
import AVFoundation
import Vision
import CoreML
import CoreGraphics
import ImageIO

public class Detector{

    //initialize the model
    let model: VNCoreMLModel!
    // create new vision request for images
    var detectionRequest: VNCoreMLRequest = {
        // Load the ML model through its generated class and create a Vision request for it.
        do {
            let model = try VNCoreMLModel(for: YOLOv3().model)
            return VNCoreMLRequest(model: model, completionHandler: nil)
        } catch {
            fatalError("can't load Vision ML model: \(error)")
        }
    }()
    
    // MARK: - Model initiation
    init(_ model: URL) throws {
        self.model = try VNCoreMLModel(for: MLModel(contentsOf: model))
    }
    
}
