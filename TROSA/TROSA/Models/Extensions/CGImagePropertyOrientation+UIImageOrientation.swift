//
//  CGImagePropertyOrientation+UIImageOrientation.swift
//  TROSA
//
//  Created by Naveen on 25/09/19.
//  Copyright © 2019 HopeResearch. All rights reserved.
//

import UIKit
import ImageIO
import CoreGraphics

extension CGImagePropertyOrientation {
    
    init(_ orientation: UIImage.Orientation) {
        switch orientation {
        case .up: self = .up
        case .upMirrored: self = .upMirrored
        case .down: self = .down
        case .downMirrored: self = .downMirrored
        case .left: self = .left
        case .leftMirrored: self = .leftMirrored
        case .right: self = .right
        case .rightMirrored: self = .rightMirrored
        default : self = .up // default orientation taken by CGImagePropertyOrientation
        }
    }
    
}
