//
//  ViewController.swift
//  TROSA
//
//  Created by Naveen on 18/09/19.
//  Copyright © 2019 HopeResearch. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import CoreML
import CoreGraphics
import ImageIO

class MainScreenVC: UIViewController  {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var galleryLabel: UILabel!
    @IBOutlet weak var galleryButton: UIBarButtonItem!
    @IBOutlet weak var cameraButton: UIBarButtonItem!
    
    //Initialozing all the sessions
    var camera: Camera!
    var imageDetector: Detector!
    var boundingBox: BoundingBox!
    var request: VNCoreMLRequest!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //initialize Model, Detector and BoundingBox class
        setUpModel()
        setUpVision()
        setUpBoundingBox()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //initialize Camera
        setUpCamera()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        camera.stop()
    }
    
    @IBAction func CameraAction(_ sender: Any) {
        //hide the imageview and enbale the camera view and perform its actions
        imageView.isHidden = true
        galleryLabel.isHidden = true
        previewView.isHidden = false
        setUpCamera()
    }
    
    @IBAction func GalleryAction(_ sender: Any) {
        //hide the camera view and enable the image view and show the gallery
        imageView.isHidden = false
        galleryLabel.isHidden = false
        previewView.isHidden = true
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        present(picker, animated: true)
    }
    
}
extension MainScreenVC{
    // MARK: - Initialization and setup of all classes
    func setUpBoundingBox(){
        boundingBox = BoundingBox(UIColor.white.cgColor, 14, UIColor.red.cgColor, UIFont(name: "Avenir", size: 14)!, 400, 100, 8, 3)
    }
    
    func setUpModel(){
        guard let modelURL = Bundle.main.url(forResource: "YOLOv3", withExtension: "mlmodelc") else{
            return
        }
        do{
            imageDetector = try Detector(modelURL)
            
        }catch let err{
            print(err.localizedDescription)
        }
        
    }
    
    func setUpVision(){
        guard let visionModel = imageDetector.model else {
            print("Error: could not create Vision model")
            return
        }
        //Creating a request for coreml using vision
        request = VNCoreMLRequest(model: visionModel, completionHandler: handleDetectionForImage)
        
        // NOTE: If you choose another crop/scale option, then you must also
        // change how the BoundingBox objects get scaled when they are drawn.
        // Currently they assume the full input image is used.
        //this is the default assumption by deafult while drawing bounding box using vision this has to be set otherwise the placement of the bounding box will be wrong
        request.imageCropAndScaleOption = .scaleFill
        
    }
    func setUpCamera(){
        camera = Camera()
        //setup Camera
        camera.delegate = self
        let success = camera.setUp(sessionPreset: AVCaptureSession.Preset.vga640x480)
        if(success == true){
            imageView.isHidden = true
            galleryLabel.isHidden = true
            if let previewLayer = self.camera.previewLayer {
                self.previewView.layer.addSublayer(previewLayer)
                
            }
            self.camera.start()
        }else{
            imageView.isHidden = false
            galleryLabel.isHidden = false
            previewView.isHidden = true
            cameraButton.isEnabled = false
            galleryLabel.text = " No Camera Found. Please pick a image from Gallery. "
            
        }
    }
    
}

extension MainScreenVC: VideoCaptureDelegate{
    
    // MARK: - Handling Of PixelBuffer(Camera Prediction)
    func videoCapture(_ capture: Camera, didCaptureVideoFrame pixelBuffer: CVPixelBuffer?) {
        if let pixelBuffer = pixelBuffer {
            // For better throughput, perform the prediction on a background queue
            // instead of on the VideoCapture queue.
            DispatchQueue.global().async {
                self.predict(pixelBuffer: pixelBuffer)
            }
        }
    }
    
    func videoCapture(_ capture: Camera, didCaptureVideoFrame: CVPixelBuffer?, timestamp: CMTime) {
        //dothe code here
    }
    
    func predict(pixelBuffer: CVPixelBuffer) {
        //Create a handler with the obtined pixelbuffer
        let handler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer)
        //perform the request
        try? handler.perform([request])
    }
    
}

extension MainScreenVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Handling Image Picker Selection
    @objc  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        let image = info[.originalImage] as! UIImage
        imageView.layer.sublayers = nil
        imageView.image = image
        //perform the classifying for the selected image
        self.ImageSelected()
    }
    
    // MARK: -  Prediction for image- Picker
    func ImageSelected(){
        galleryLabel.text = " Classifying...."
        guard let image = imageView.image else {
            return
        }
        //converting UIImage into CIImage
        guard let ciImage = CIImage(image: image)
            else { fatalError("can't create CIImage from UIImage") }
        //find the orientation of the image
        let orientation = CGImagePropertyOrientation(image.imageOrientation)
        // Show the image in the UI.
        imageView.image = image
        // Run the  detector
        let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation)
        DispatchQueue.global(qos: .userInteractive).async {
            do {
                //perform the request , once the request is completed it hits the handleDetection
                try handler.perform([self.request])
                
            } catch {
                print(error)
            }
        }
    }
    
}
extension MainScreenVC{
    
    // MARK: - Handle Detection and Draw bounding Box
    func handleDetectionForImage(request: VNRequest, error: Error?){
        //get the results for the request
        guard let results = request.results, !results.isEmpty else { return  }
        //find the observation using the Vision recognized object observation
        for observation in results where observation is VNRecognizedObjectObservation {
            guard let observation = observation as? VNRecognizedObjectObservation else {
                continue
            }
            DispatchQueue.main.async {
                // Label with highest confidence
                let bestLabel = observation.labels.first!
                //get the bounding box element of observation
                let objectBounds = observation.boundingBox
                DispatchQueue.main.async { [unowned self] in
                    //create a image rectangle for the observation
                    let boundingBoxRect = VNImageRectForNormalizedRect(objectBounds, Int(self.imageView!.frame.width), Int(self.imageView!.frame.height))
                    //initialize the bounding box class
                    var yval = self.imageView!.bounds.midY-boundingBoxRect.midY+200
                    var xval = boundingBoxRect.midX
                    //create shape layer using bounding box
                    let shapelayer = self.boundingBox.createRoundedRectSubLayer(boundingBoxRect, xval, yval)
                    self.imageView!.layer.addSublayer(shapelayer)
                    xval = boundingBoxRect.origin.x-2
                    yval = self.imageView!.frame.origin.y-boundingBoxRect.origin.y+188
                    //create text layer using bouding box
                    let textlayer = self.boundingBox.createTextSubLayer(boundingBoxRect, "\(bestLabel.identifier) \(bestLabel.confidence)", xval, yval)
                    self.imageView!.layer.addSublayer(textlayer)
                }
                print(bestLabel.identifier, observation.confidence, objectBounds)
                self.galleryLabel.text = " Classification Done. "
            }
        }
    }
    
}
